<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Avalanche\Bundle\ImagineBundle\AvalancheImagineBundle(),

            // bloggist bundles
            new Bloggist\Bundle\FacebookBundle\BloggistFacebookBundle(),
            new Bloggist\Bundle\GoogleBundle\BloggistGoogleBundle(),
            new Bloggist\Bundle\PluginBundle\BloggistPluginBundle(),
            new Bloggist\Bundle\PostAdminBundle\BloggistPostAdminBundle(),
            new Bloggist\Bundle\PostDoctrineBundle\BloggistPostDoctrineBundle(),
            new Bloggist\Bundle\PostRssBundle\BloggistPostRssBundle(),
            new Bloggist\Bundle\PostWebBundle\BloggistPostWebBundle(),
            new Bloggist\Bundle\ImageBundle\BloggistImageBundle(),
            new Bloggist\Bundle\PageBundle\BloggistPageBundle(),
            new Bloggist\Bundle\MenuBundle\BloggistMenuBundle(),
            new Bloggist\Bundle\SpotBundle\BloggistSpotBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
