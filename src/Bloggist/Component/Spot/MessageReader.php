<?php

namespace Bloggist\Component\Spot;

/**
 * Description of MessageReader
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class MessageReader implements MessageReaderInterface
{
    private $url = 'https://api.findmespot.com/spot-main-web/consumer/rest-api/2.0/public/feed/{gid}/message.json';
    private $password;
    
    public function __construct($gid, $password = null)
    {
        $this->password = $password;
        $this->url = \str_replace('{gid}', $gid, $this->url);
        if ($password) {
            $this->url .= '?feedPassword=' . $password;
        }        
    }
    
    public function getPage($pageNumber)
    {
        $url = $this->url;
        
        if ($pageNumber > 1) {
            $start = ($pageNumber - 1) * 50 + 1;
            if ($this->password) {
                $url .= '&start=' . $start;
            } else {
                $url .= '?start=' . $start;
            }
        }

        $level = error_reporting(0);
        $content = file_get_contents($url);
        error_reporting($level);
        if (false === $content) {
            $error = error_get_last();
            throw new \RuntimeException($error['message']);
        }
        
        $content = \json_decode($content, true);

        $messageMapper = new MessageMapper;
        $messages = array();
        
        if (\array_key_exists('errors', $content['response'])
                && $content['response']['errors']['error']['code'] == 'E-0195') {
            return array();
        }
        
        if ($content['response']['feedMessageResponse']['count'] > 0) {
            foreach ($content['response']['feedMessageResponse']['messages']['message'] as $arr) {
                $message = new Message();
                $messageMapper->fromArray($message, $arr);
                $messages[] = $message;
            }
        }
        
        return $messages;
    }

}