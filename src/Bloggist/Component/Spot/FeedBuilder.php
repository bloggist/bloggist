<?php

namespace Bloggist\Component\Spot;

use Bloggist\Component\Rss\FeedBuilderInterface;
use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Suin\RSSWriter\Item;

/**
 * Description of FeedBuilder
 *
 * @author zxc
 */
class FeedBuilder implements FeedBuilderInterface
{
    private $messageReader;
    private $channel;
    private $itemTitle;

    public function __construct(MessageReaderInterface $messageReader, Channel $channel, $itemTitle)
    {
        $this->messageReader = $messageReader;
        $this->channel = $channel;
        $this->itemTitle = $itemTitle;
    }
    
    public function build()
    {
        $messages = $this->messageReader->getPage(1);
        
        $channel = clone $this->channel;
        $feed = new Feed;
        $feed->addChannel($channel);
        
        for ($i = 0; $i < \count($messages); $i++) {
            $message = $messages[$i];
            /* @var $message Message */
            $item = new Item;
            $item->title($this->itemTitle);
            $url = 'http://maps.google.com/maps?f=q&hl=en&geocode=&q=' .
                    $message->getLatitude() . ',' .
                    $message->getLongitude() . '&z=12';
            $item->url($url);
            $item->guid($url . '&mid=' . $message->getForeignId());
            $item->description($url);
            $channel->addItem($item);
        }
        
        return $feed;
    }
}