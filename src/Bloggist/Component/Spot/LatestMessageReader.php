<?php

namespace Bloggist\Component\Spot;

use Symfony\Component\Finder\SplFileInfo;

/**
 * Description of MessageReader
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class LatestMessageReader
{
    private $url = 'https://api.findmespot.com/spot-main-web/consumer/rest-api/2.0/public/feed/{gid}/latest.json';

    public function getMessage($gid, $password = null)
    {
        $url = \str_replace('{gid}', $gid, $this->url);
        if ($password) {
            $url .= '?feedPassword=' . $password;
        }

        $level = error_reporting(0);
        $content = file_get_contents($url);
        error_reporting($level);
        if (false === $content) {
            $error = error_get_last();
            throw new \RuntimeException($error['message']);
        }
        
        $content = \json_decode($content, true);

        if ($content['response']['feedMessageResponse']['count'] > 0) {
            return $content['response']['feedMessageResponse']['messages']['message'];
        } else {
            return null;
        }
    }

}