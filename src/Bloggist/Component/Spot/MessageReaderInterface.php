<?php

namespace Bloggist\Component\Spot;

/**
 * Description of MessageReaderInterface
 *
 * @author zxc
 */
interface MessageReaderInterface
{

    public function getPage($pageNumber);
    
}