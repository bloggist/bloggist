<?php

namespace Bloggist\Component\Spot;

/**
 * Description of Message
 *
 * @author zxc
 */
class Message
{

    private $id;
    private $foreignId;
    private $messengerId;
    private $unixTime;
    private $messageType;
    private $latitude;
    private $longitude;
    private $showCustomMsg;
    private $altitude;
    private $hidden;
    private $content;
    
    public function getId()
    {
        return $this->id;
    }

    public function getForeignId()
    {
        return $this->foreignId;
    }

    public function setForeignId($foreignId)
    {
        $this->foreignId = $foreignId;
    }

    public function getMessengerId()
    {
        return $this->messengerId;
    }

    public function setMessengerId($messengerId)
    {
        $this->messengerId = $messengerId;
    }

    public function getUnixTime()
    {
        return $this->unixTime;
    }

    public function setUnixTime($unixTime)
    {
        $this->unixTime = $unixTime;
    }

    public function getMessageType()
    {
        return $this->messageType;
    }

    public function setMessageType($messageType)
    {
        $this->messageType = $messageType;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    public function getShowCustomMsg()
    {
        return $this->showCustomMsg;
    }

    public function setShowCustomMsg($showCustomMsg)
    {
        $this->showCustomMsg = $showCustomMsg;
    }

    public function getAltitude()
    {
        return $this->altitude;
    }

    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;
    }

    public function getHidden()
    {
        return $this->hidden;
    }

    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

}