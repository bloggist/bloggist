<?php

namespace Bloggist\Component\Spot;

use Doctrine\ORM\EntityManager;

/**
 * Description of DoctrineLatestMessageReader
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class DoctrineLatestMessageReader
{
    /**
     * @var EntityManager
     */
    private $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getMessage()
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('m')
                ->from('Bloggist\Component\Spot\Message', 'm')
                ->orderBy('m.unixTime', 'DESC')
                ->setMaxResults(1);
        
        return $qb->getQuery()->getOneOrNullResult();
    }
    
}