<?php

namespace Bloggist\Component\Spot;

/**
 * Description of MessageMapper
 *
 * @author zxc
 */
class MessageMapper
{

    public function fromArray(Message $message, $arr)
    {
        $message->setForeignId($arr['id']);
        $message->setMessengerId($arr['messengerId']);
        $message->setUnixTime((int)$arr['unixTime']);
        $message->setMessageType($arr['messageType']);
        $message->setLatitude((double)$arr['latitude']);
        $message->setLongitude((double)$arr['longitude']);
        $message->setShowCustomMsg((bool)($arr['showCustomMsg'] == 'Y'));
        $message->setAltitude((int)$arr['altitude']);
        $message->setHidden((bool)$arr['hidden']);
        if (\array_key_exists('content', $arr)) {
            $message->setContent($arr['content']);
        }
        
        return $message;
    }

}