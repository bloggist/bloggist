<?php

namespace Bloggist\Component\Spot;

use Doctrine\ORM\EntityManager;

/**
 * Description of DoctrineMessageReader
 *
 * @author zxc
 */
class DoctrineMessageReader implements MessageReaderInterface
{
    private $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getPage($pageNumber)
    {
        $pageSize = 50;
        
        $qb = $this->em->createQueryBuilder();
        $qb->select('m')
                ->from('Bloggist\Component\Spot\Message', 'm')
                ->orderBy('m.unixTime', 'DESC')
                ->setMaxResults($pageSize)
                ->setFirstResult(($pageNumber - 1) * $pageSize);
        
        return $qb->getQuery()->getResult();
    }

}