<?php

namespace Bloggist\Component\Service;

/**
 * Description of FilterInterface
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface PageInterface
{

    public function getIndex();

    public function getSize();

}

