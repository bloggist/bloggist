<?php

namespace Bloggist\Component\Service;

/**
 * Description of ResultPageInterface
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface ResultPageInterface extends PageInterface
{

    public function countNumberOfPages();

    public function countItemsOnPage();

    public function countItems();

    public function getItems();

}