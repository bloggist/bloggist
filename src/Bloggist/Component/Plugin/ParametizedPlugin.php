<?php

namespace Bloggist\Component\Plugin;

use Symfony\Component\Templating\EngineInterface;

/**
 * ParametizedPlugin
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class ParametizedPlugin implements RenderablePluginInterface
{

    /**
     * @var EngineInterface
     */
    private $templatingEngine;

    /**
     * @var string
     */
    private $template;

    /**
     * @var ParametersResolverInterface
     */
    private $parametersResolver;

    public function __construct(
            EngineInterface $templating,
            $template,
            ParametersResolverInterface $parametersResolver)
    {
        $this->templatingEngine = $templating;        
        $this->template = $template;
        $this->parametersResolver = $parametersResolver;
    }
    
    public function render(/* object */ $subject, array $params = array())
    {
        $params = \array_merge($params, $this->parametersResolver->resolveParameters($subject));

        return $this->templatingEngine->render($this->template, $params);
    }

}