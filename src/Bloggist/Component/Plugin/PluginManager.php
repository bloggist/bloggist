<?php

namespace Bloggist\Component\Plugin;

use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * PluginManager
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PluginManager extends ContainerAware implements PluginManagerInterface
{

    const DELIMITER = \PHP_EOL;

    private $plugins = array();

    public function registerPlugin($area, $id, array $params = array())
    {
        if (!\array_key_exists($area, $this->plugins)) {
            $this->plugins[$area] = array();
        }

        $this->plugins[$area][] = array('id' => $id, 'params' => $params);
    }

    public function displayArea($name, $subject = null)
    {
        if (!\array_key_exists($name, $this->plugins)) {
            return '';
        }

        $results = array();
        foreach ($this->plugins[$name] as $plugin) {
            /* @var $plugin RenderablePluginInterface */
            $results[] = $this->container->get($plugin['id'])->render($subject, $plugin['params']);
        }

        return \implode(self::DELIMITER, $results);
    }

}