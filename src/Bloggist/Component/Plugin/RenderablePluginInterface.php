<?php

namespace Bloggist\Component\Plugin;

/**
 * RenderablePluginInterface
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface RenderablePluginInterface
{

    public function render(/* object */ $subject, array $params = array());

}