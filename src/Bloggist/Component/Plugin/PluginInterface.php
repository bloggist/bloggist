<?php

namespace Bloggist\Component\Plugin;

/**
 * Description of PluginInterface
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface PluginInterface
{

    public function getName();
    
}