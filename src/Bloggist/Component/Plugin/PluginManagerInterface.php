<?php

namespace Bloggist\Component\Plugin;

/**
 * Description of PluginManagerInterface
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface PluginManagerInterface
{

    public function displayArea($name, $subject = null);
    
}