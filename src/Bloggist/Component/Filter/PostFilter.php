<?php

namespace Bloggist\Component\Filter;

/**
 * Description of PostFilter
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostFilter implements PostFilterInterface
{
    private $filters = array();

    public function getStatuses()
    {
        return $this->hasStatuses() ? $this->filters['statuses'] : null;
    }

    public function hasStatuses()
    {
        return \array_key_exists('statuses', $this->filters);
    }

    public function setStatuses(array $statuses)
    {
        $this->filters['statuses'] = $statuses;
    }

    public function getPostedBefore()
    {
        return $this->hasPostedBefore() ? $this->filters['posted_before'] : null;
    }

    public function hasPostedBefore()
    {
        return \array_key_exists('posted_before', $this->filters);
    }

    public function setPostedBefore(\DateTime $postedBefore)
    {
        $this->filters['posted_before'] = $postedBefore;
    }

    public function getPostedAfter()
    {
        return $this->hasPostedAfter() ? $this->filters['posted_after'] : null;
    }

    public function hasPostedAfter()
    {
        return \array_key_exists('posted_after', $this->filters);
    }

}