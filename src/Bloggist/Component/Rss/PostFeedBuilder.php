<?php

namespace Bloggist\Component\Rss;

use Bloggist\Component\Filter\PostFilterInterface;
use Bloggist\Component\Routing\RouterProxyInterface;
use Bloggist\Component\Service\Page;
use Bloggist\Component\Service\ServiceInterface;
use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Suin\RSSWriter\Item;

/**
 * Description of FeedBuilder
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostFeedBuilder implements FeedBuilderInterface
{
    const DEFAULT_PAGE = 1;
    const DEFAULT_SIZE = 10;
    
    /**
     * @var PostFilterInterface
     */
    private $filter;

    /**
     * @var ServiceInterface
     */
    private $service;

    /**
     * @var Channel
     */
    private $channel;

    /**
     * @var RouterProxyInterface
     */
    private $routerProxy;

    public function __construct(
            ServiceInterface $service,
            PostFilterInterface $filter,
            Channel $channel,
            RouterProxyInterface $routerProxy)
    {
        $this->filter = $filter;
        $this->service = $service;
        $this->channel = $channel;
        $this->routerProxy = $routerProxy;
    }

    public function build()
    {
        $channel = clone $this->channel;
        $feed = new Feed;
        $feed->addChannel($channel);

        $page = new Page(self::DEFAULT_PAGE, self::DEFAULT_SIZE);
        $this->filter->setPostedBefore(new \DateTime);
        
        $posts = $this->service->paginate($page, $this->filter, 'postedAt', 'DESC')->getItems();

        if (\count($posts) > 0) {
            $channel->pubDate($posts[0]->getPostedAt()->getTimestamp());
        }

        foreach ($posts as $post) {
            /* @var $post \Bloggist\Component\Entity\Post */
            $item = new Item;
            $item->title($post->getTitle());
            $item->pubDate($post->getPostedAt()->getTimestamp());
            if ($post instanceof \Bloggist\Component\Entity\ArticlePost) {
                $item->description(\strip_tags($post->getSynopsis()));
            }
            $url = $this->routerProxy->generate($post, true);
            $item->url($url);
            $item->guid($url);
            $item->appendTo($channel);
        }

        return $feed;
    }
}