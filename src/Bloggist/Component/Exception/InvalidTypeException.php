<?php

namespace Bloggist\Component\Exception;

/**
 * Description of InvalidType
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class InvalidTypeException extends \InvalidArgumentException
{
    private $messageTemplate = "PostRouterProxy requires object of type %s. Object of type %s given.";

    public function __construct($expectedType, $object) {
        $message = \sprintf($this->messageTemplate, $expectedType, $object);
        parent::__construct($message);
    }

}