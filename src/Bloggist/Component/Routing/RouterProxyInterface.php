<?php

namespace Bloggist\Component\Routing;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface RouterProxyInterface
{

    public function handles($object);

    public function generate($object, $absolute = false);

}
