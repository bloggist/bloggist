<?php

namespace Bloggist\Component\Entity;

/**
 * Description of Tag
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class Tag
{

    private $id;
    private $name;
    private $post;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPost()
    {
        return $this->post;
    }

    public function setPost($post)
    {
        $this->post = $post;
    }

    public function __toString()
    {
        return $this->getName();
    }

}