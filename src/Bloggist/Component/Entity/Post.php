<?php

namespace Bloggist\Component\Entity;

use Bloggist\Component\Entity\Status\Status;
use Bloggist\Component\Entity\Status\StatusInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Bloggist\Component\Entity\Post
 */
abstract class Post
{
    /**
     * @var string $title
     */
    private $title;
    
    /**
     * @var string $synopsis
     */
    private $synopsis;

    /**
     * @var StatusInterface $status
     */
    private $status;

    /**
     * @var string $slug
     */
    private $slug;

    /**
     * @var ArrayCollection
     */
    private $tags;

    /**
     * @var \DateTime $createdAt
     */
    private $createdAt;

    /**
     * @var \DateTime $updatedAt
     */
    private $updatedAt;

    /**
     * @var \DateTime $postedAt
     */
    private $postedAt;

    /**
     * @var integer $id
     */
    private $id;

    private $type;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Get synopsis
     *
     * @return string
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * Set synopsis
     *
     * @param string $synopsis
     * @return ArticlePost
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * Set status
     *
     * @param StatusInterface $status
     * @return Post
     */
    public function setStatus(StatusInterface $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return StatusInterface
     */
    public function getStatus()
    {
        if (\is_string($this->status)) {
            return new Status($this->status);
        }
        return $this->status;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set postedAt
     *
     * @param \DateTime $postedAt
     * @return Post
     */
    public function setPostedAt($postedAt)
    {
        $this->postedAt = $postedAt;

        return $this;
    }

    /**
     * Get postedAt
     *
     * @return \DateTime
     */
    public function getPostedAt()
    {
        return $this->postedAt;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $tags
     */
    public function setTags(ArrayCollection $tags)
    {
        foreach ($this->tags as $t) {
            $callback = function($t2) use ($t) { return \mb_strtolower($t->getName()) == \mb_strtolower($t2->getName()); };
            $existing = \array_filter($tags->toArray(), $callback);
            if (count($existing) > 0) {
                foreach ($existing as $t2) {
                    $t->setName($t2->getName());
                    $tags->removeElement($t2);
                }
            } else {
                $this->tags->removeElement($t);
            }
        }

        foreach ($tags as $tag) {
            $tag->setPost($this);
            $this->tags->add($tag);
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    abstract public function getType();

}