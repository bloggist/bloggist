<?php

namespace Bloggist\Component\Entity;

/**
 * Bloggist\Component\Entity\MediaPost
 */
class MediaPost extends Post
{
    const TYPE = 'Media';

    /**
     * @var string $sourceType
     */
    private $sourceType;

    /**
     * @var string $resourceId
     */
    private $resourceId;

    /**
     * @var string $altText
     */
    private $altText;

    /**
     * @var string $caption
     */
    private $caption;

    /**
     * @var string $copyright
     */
    private $copyright;

    /**
     * Set sourceType
     *
     * @param string $sourceType
     * @return MediaPost
     */
    public function setSourceType($sourceType)
    {
        $this->sourceType = $sourceType;

        return $this;
    }

    /**
     * Get sourceType
     *
     * @return string
     */
    public function getSourceType()
    {
        return $this->sourceType;
    }

    /**
     * Get resource id
     *
     * @return string
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * Set resource id.
     *
     * @param string $resourceId
     * @return \Bloggist\Component\Entity\MediaPost
     */
    public function setResourceId($resourceId)
    {
        $this->resourceId = $resourceId;

        return $this;
    }

    /**
     * Set altText
     *
     * @param string $altText
     * @return MediaPost
     */
    public function setAltText($altText)
    {
        $this->altText = $altText;

        return $this;
    }

    /**
     * Get altText
     *
     * @return string
     */
    public function getAltText()
    {
        return $this->altText;
    }

    /**
     * Set caption
     *
     * @param string $caption
     * @return MediaPost
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set copyright
     *
     * @param string $copyright
     * @return MediaPost
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;

        return $this;
    }

    /**
     * Get copyright
     *
     * @return string
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    public function getType()
    {
        return self::TYPE;
    }
}