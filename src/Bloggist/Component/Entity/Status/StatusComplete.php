<?php

namespace Bloggist\Component\Entity\Status;

/**
 * Description of StatusComplete
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class StatusComplete extends Status
{
    const NAME = 'complete';
    
    public function __construct()
    {
        parent::__construct(self::NAME);
    }
}