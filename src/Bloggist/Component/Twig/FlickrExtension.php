<?php

namespace Bloggist\Component\Twig;

/**
 * Description of DateTimeExtension
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class FlickrExtension extends \Twig_Extension
{
    const REGEXP = '/\/(\d+)_/';
    const BASE_URL = 'http://flickr.com/photos/';
    
    private $username;
    
    public function __construct($username) 
    {
        $this->username = $username;
    }

    public function getFilters()
    {
        return array(
            'flickr_photo_link' => new \Twig_Filter_Method($this, 'flickrPhotoLink'),
        );
    }

    public function flickrPhotoLink($resourceId)
    {
        $matches = array();
        preg_match(self::REGEXP, $resourceId, $matches);
        
        if (\count($matches) > 1) {
            return self::BASE_URL . $this->username . '/' . $matches[1] . '/';
        } else {
            throw new \InvalidArgumentException("Incorrect flickr resource id ($resourceId)");
        }
    }

    public function getName()
    {
        return 'bloggist_flickr_extension';
    }

}
