<?php

namespace Bloggist\Component\Twig;

/**
 * Description of DateTimeExtension
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class DateTimeExtension extends \Twig_Extension
{
    private $locale;
    private $datetype;
    private $timetype;
    private $timezone;

    public function __construct($locale,
            $datetype = \IntlDateFormatter::FULL,
            $timetype = \IntlDateFormatter::FULL,
            $timezone = 'GMT')
    {
        $this->locale = $locale;
        $this->datetype = $datetype;
        $this->timetype = $timetype;
        $this->timezone = $timezone;
    }

    public function getFilters()
    {
        return array(
            'format_date' => new \Twig_Filter_Method($this, 'formatDate', array('is_safe' => array('html'))),
            'format_time' => new \Twig_Filter_Method($this, 'formatTime', array('is_safe' => array('html'))),
            'format_datetime' => new \Twig_Filter_Method($this, 'formatDateTime', array('is_safe' => array('html')))
        );
    }

    public function formatDate(\DateTime $date)
    {
        $formatter = new \IntlDateFormatter($this->locale, $this->datetype, \IntlDateFormatter::NONE, $this->timezone);
        return $formatter->format($date->getTimestamp());
    }

    public function formatTime(\DateTime $date)
    {
        $formatter = new \IntlDateFormatter($this->locale, \IntlDateFormatter::NONE, $this->timetype, $this->timezone);
        return $formatter->format($date->getTimestamp());
    }

    public function formatDateTime(\DateTime $date)
    {
        $formatter = new \IntlDateFormatter($this->locale, $this->datetype, $this->timetype, $this->timezone);
        return $formatter->format($date->getTimestamp());
    }

    public function getName()
    {
        return 'bloggist_datetime_extension';
    }

}