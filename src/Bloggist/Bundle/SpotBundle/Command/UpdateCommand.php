<?php

namespace Bloggist\Bundle\SpotBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of UpdateCommand
 *
 * @author zxc
 */
class UpdateCommand extends ContainerAwareCommand
{
    
    public function configure()
    {
        $this->setName('bloggist:spot:update');
    }
    
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $messageReader = $this->getContainer()->get('bloggist.spot.message_reader');
        
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        /* @var $qb \Doctrine\ORM\QueryBuilder */
        $qb = $em->createQueryBuilder();
        $qb->select('m')
                ->from('Bloggist\Component\Spot\Message', 'm')
                ->orderBy('m.unixTime', 'DESC')
                ->setMaxResults(1);
        
        $latestMessage = $qb->getQuery()->getOneOrNullResult();
        
        if ($latestMessage) {
            $latestUnitTime = $latestMessage->getUnixTime();
        } else {
            $latestUnitTime = null;
        }
        
        $page = 1;
        do {
            $messages = $messageReader->getPage($page);
            foreach ($messages as $message) {
                if ($latestUnitTime !== null && $message->getUnixTime() <= $latestUnitTime) {
                    $em->flush();
                    return;
                } else {
                    $em->persist($message);
                }
            }            
            $page++;
        } while (\count($messages));
        
        $em->flush();
    }
    
}