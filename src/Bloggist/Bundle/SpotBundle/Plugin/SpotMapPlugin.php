<?php

namespace Bloggist\Bundle\SpotBundle\Plugin;

use Bloggist\Component\Spot\DoctrineLatestMessageReader;
use Symfony\Component\Templating\EngineInterface;

/**
 * Description of SpotMapPlugin
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class SpotMapPlugin
{

    /**
     * @var DoctrineLatestMessageReader
     */
    private $messageReader;
    
    /**
     * @var EngineInterface
     */
    private $templatingEngine;

    /**
     * @var string
     */
    private $template;

    public function __construct(
            DoctrineLatestMessageReader $messageReader,
            EngineInterface $templating,
            $template)
    {
        $this->messageReader = $messageReader;
        $this->templatingEngine = $templating;
        $this->template = $template;
    }

    public function render(/* object */ $subject, array $params = array())
    {
        if ($params['type'] == 'div') {
            $message = $this->messageReader->getMessage();

            if ($message) {
                return $this->templatingEngine->render($this->template, array('subject' => $subject, 'message' => $message, 'type' => $params['type']));
            } else {
                return '';
            }
        } else {
            return $this->templatingEngine->render($this->template, array('subject' => $subject, 'type' => $params['type']));            
        }

    }

}