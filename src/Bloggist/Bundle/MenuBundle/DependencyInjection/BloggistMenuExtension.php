<?php

namespace Bloggist\Bundle\MenuBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class BloggistMenuExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');


        $elements = array();
        foreach ($config['menu'] as $item) {
            $element = array('title' => $item['title'], 'type' => $item['type'], 'value' => $item['value'], 'parameters' => array());
            foreach ($item['parameters'] as $parameter) {
                $element['parameters'][$parameter['key']] = $parameter['value'];
            }
            $elements[] = $element;
        }

        $container->setParameter('bloggist.menu.elements', $elements);
    }
}
