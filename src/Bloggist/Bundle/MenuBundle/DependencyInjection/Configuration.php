<?php

namespace Bloggist\Bundle\MenuBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('bloggist_menu');

        $rootNode->addDefaultsIfNotSet(array('menu' => array()))
                 ->children()
                    ->arrayNode('menu')
                        ->prototype('array')
                            ->children()
                                ->scalarNode('title')->end()
                                ->scalarNode('type')
                                    ->defaultValue('internal')
                                    ->validate()
                                        ->ifNotInArray(array('internal', 'external'))
                                        ->thenInvalid('Type must be either \'internal\' or \'external\'')
                                    ->end()
                                ->end()
                                ->scalarNode('value')->end()
                                ->arrayNode('parameters')
                                    ->prototype('array')
                                        ->children()
                                            ->scalarNode('key')->end()
                                            ->scalarNode('value')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                 ->end();

        return $treeBuilder;
    }
}
