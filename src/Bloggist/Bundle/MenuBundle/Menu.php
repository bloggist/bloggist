<?php

namespace Bloggist\Bundle\MenuBundle;

use Bloggist\Component\Plugin\RenderablePluginInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Description of Menu
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class Menu implements RenderablePluginInterface
{

    private $elements;
    private $router;
    private $templating;
    private $template = 'BloggistMenuBundle:Menu:show.html.twig';

    public function __construct(array $elements, RouterInterface $router, EngineInterface $templating)
    {
        $this->elements = $elements;
        $this->router = $router;
        $this->templating = $templating;
    }

    public function render($subject, array $params = array())
    {
        return $this->templating->render(
                $this->template,
                array(
                        'elements' => $this->elements,
                        'params' => $params,
                        'subject' => $subject
                )
        );
    }

}