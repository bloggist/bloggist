<?php

namespace Bloggist\Bundle\ImageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of ImageType
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class ImageType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title');
        $builder->add('credit');
        $builder->add('file', 'file', array('required' => true));
    }

    public function getName()
    {
        return 'image';
    }
}