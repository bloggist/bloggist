<?php

namespace Bloggist\Bundle\ImageBundle\Entity\Repository;

use Bloggist\Bundle\ImageBundle\Filter\ImageFilter;
use Bloggist\Component\Service\PageInterface;
use Bloggist\Component\Service\ResultPage;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ImageRepository
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class ImageRepository extends EntityRepository
{
    
    public function find($id)
    {
        return parent::find($id);
    }

    public function index(ImageFilter $filter, $sort, $order)
    {
        return $this->buildQueryBuilder($filter, $sort, $order)->getQuery()->getResult();
    }

    public function paginate(PageInterface $page, ImageFilter $filter, $sort, $order)
    {
        $qb = $this->buildQueryBuilder($filter, $sort, $order);

        $countQb = clone $qb;
        $itemsCount = $countQb->select('count(i)')->getQuery()->getResult();

        $qb->setMaxResults($page->getSize());
        $qb->setFirstResult(($page->getIndex() - 1) * $page->getSize());

        return new ResultPage($page, $qb->getQuery()->getResult(), $itemsCount);
    }

    public function buildQueryBuilder(ImageFilter $filter, $sort, $order)
    {
        $qb = $this->createQueryBuilder('i');

        $qb->orderBy('i.' . $sort, $order);

        return $qb;
    }

}