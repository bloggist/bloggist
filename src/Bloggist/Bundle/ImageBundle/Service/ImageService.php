<?php

namespace Bloggist\Bundle\ImageBundle\Service;

use Imagine\Image\ImagineInterface;
use Bloggist\Bundle\ImageBundle\Entity\Image;
use Bloggist\Bundle\ImageBundle\Entity\Repository\ImageRepository;
use Bloggist\Component\Filter\FilterInterface;
use Bloggist\Component\Service\PageInterface;
use Bloggist\Component\Service\ServiceInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Description of ImageService
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class ImageService implements ServiceInterface
{
    private $imageRepository;

    private $entityManager;

    /**
     * @var ImagineInterface
     */
    private $imagine;

    private $storagePath;

    public function __construct(
            ImageRepository $imageRepository,
            EntityManager $entityManager,
            ImagineInterface $imagine,
            $storagePath)
    {
        $this->imageRepository = $imageRepository;
        $this->entityManager = $entityManager;
        $this->imagine = $imagine;
        $this->storagePath = $storagePath;
    }

    public function find($identifier)
    {
        return $this->imageRepository->find($identifier);
    }

    public function index(FilterInterface $filter, $sort, $order)
    {
        return $this->imageRepository->index($filter, $sort, $order);
    }

    public function paginate(PageInterface $page, FilterInterface $filter, $sort, $order)
    {
        return $this->imageRepository->paginate($page, $filter, $sort, $order);
    }

    public function store(Image $image, UploadedFile $file)
    {
        $img = $this->imagine->open($file->getRealPath());

        $image->setOriginalFilename($file->getClientOriginalName());
        $image->setSize($file->getClientSize());
        $image->setWidth($img->getSize()->getWidth());
        $image->setHeight($img->getSize()->getHeight());
        if ($file->guessExtension()) {
            $image->setExtension($file->guessExtension());
        } else {
            $image->setExtension('bin');
        }
        
        $this->entityManager->beginTransaction();

        $this->entityManager->persist($image);
        $this->entityManager->flush();
        $file->move($this->storagePath, $image->getId() . '.' . $image->getExtension());

        $this->entityManager->commit();
    }

}