<?php

namespace Bloggist\Bundle\ImageBundle\Controller;

use Bloggist\Bundle\ImageBundle\Entity\Image;
use Bloggist\Component\Service\ServiceInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Templating\EngineInterface;

/**
 * Description of ImageController
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class ImageController
{
    private $request;

    private $formFactory;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var ServiceInterface
     */
    private $imageService;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var string
     */
    private $formTemplate;

    /**
     * @var string
     */
    private $indexTemplate;

    public function __construct(
            Request $request,
            FormFactory $formFactory,
            EngineInterface $templating,
            ServiceInterface $imageService,
            RouterInterface $router,
            /* string */ $formTemplate,
            /* string */ $indexTemplate)
    {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->templating = $templating;
        $this->imageService = $imageService;
        $this->router = $router;
        $this->formTemplate = $formTemplate;
        $this->indexTemplate = $indexTemplate;
    }

    public function index()
    {
        $images = $this->imageService->index(new \Bloggist\Bundle\ImageBundle\Filter\ImageFilter, 'id', 'desc');

        return $this->templating->renderResponse($this->indexTemplate, array('images' => $images));
    }

    public function create()
    {
        $form = $this->formFactory->create(new \Bloggist\Bundle\ImageBundle\Form\ImageType);

        if ($this->request->isMethod('post')) {
            $form->bindRequest($this->request);

            if ($form->isValid()) {
                $data = $form->getData();
                $image = new Image;
                $image->setTitle($data['title']);
                $image->setCredit($data['credit']);

                $this->imageService->store($image, $form['file']->getData());
            }

            $redirectUrl = $this->router->generate('bloggist_image_admin_list');

            return new RedirectResponse($redirectUrl, 302);
        }

        return $this->templating->renderResponse($this->formTemplate, array('form' => $form->createView()));
    }

}