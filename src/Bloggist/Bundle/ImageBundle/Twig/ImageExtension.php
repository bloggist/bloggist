<?php

namespace Bloggist\Bundle\ImageBundle\Twig;

use Bloggist\Bundle\ImageBundle\Entity\Image;
use Bloggist\Component\Service\ServiceInterface;

/**
 * Description of ImageExtension
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class ImageExtension extends \Twig_Extension
{
    private $webPath;
    private $service;

    public function __construct($webPath, ServiceInterface $service)
    {
        $this->webPath = $webPath;
        $this->service = $service;
    }

    public function getFilters()
    {
        return array(
            'image_web_path' => new \Twig_Filter_Method($this, 'getWebPath'),
            'image_web_path_by_id' => new \Twig_Filter_Method($this, 'getWebPathById'),
        );
    }

    public function getWebPath(Image $image)
    {
        return $this->webPath . '/' . $image->getId() . '.' . $image->getExtension();
    }

    public function getWebPathById($id)
    {
        $image = $this->service->find($id);

        return $this->getWebPath($image);
    }

    public function getName()
    {
        return 'bloggist_image_extension';
    }

}