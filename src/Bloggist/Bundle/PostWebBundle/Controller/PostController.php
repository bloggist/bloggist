<?php

namespace Bloggist\Bundle\PostWebBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

use Bloggist\Component\Entity\Blog;
use Bloggist\Component\Filter\PostedPostFilter;
use Bloggist\Component\Service\Page;
use Bloggist\Bundle\PostDoctrineBundle\Service\PostService;
use Bloggist\Bundle\PostWebBundle\Templating\PostRendererInterface;

class PostController
{
    const DEFAULT_PAGE = 1;
    const DEFAULT_SIZE = 10;

    private $request;

    /**
     * @var \Bloggist\Bundle\PostDoctrineBundle\Service\PostService
     */
    private $service;

    /**
     * @var PostRendererInterface
     */
    private $postRenderer;

    /**
     * @var Symfony\Component\Templating\EngineInterface
     */
    private $templating;

    private $indexTemplate;

    private $showTemplate;

    public function __construct(Request $request,
            PostService $service,
            EngineInterface $templating,
            PostRendererInterface $postRenderer,
            $indexTemplate,
            $showTemplate)
    {
        $this->request = $request;
        $this->service = $service;
        $this->templating = $templating;
        $this->postRenderer = $postRenderer;
        $this->indexTemplate = $indexTemplate;
        $this->showTemplate = $showTemplate;
    }

    public function index($page = null)
    {
        if (null === $page) {
            $page = self::DEFAULT_PAGE;
        }

        $page = new Page($page, self::DEFAULT_SIZE);

        $filter = new PostedPostFilter;
        $posts = $this->service->paginate($page, $filter, 'postedAt', 'DESC');

        $renderedPosts = array();

        $this->postRenderer->setStubMode(true);

        foreach ($posts->getItems() as $post) {
            $renderedPosts[] = $this->postRenderer->render($post);
        }

        return $this->templating->renderResponse($this->indexTemplate, array('posts' => $renderedPosts, 'subject' => new Blog));
    }

    public function show($slug)
    {
        $post = $this->service->findOneBySlug($slug);

        if (empty($post)) {
            throw new NotFoundHttpException();
        }

        $renderedPost = $this->postRenderer->render($post);

        return $this->templating->renderResponse($this->showTemplate, array('post' => $renderedPost, 'subject' => $post));
    }
}
