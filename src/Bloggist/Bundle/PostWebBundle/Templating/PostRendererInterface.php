<?php

namespace Bloggist\Bundle\PostWebBundle\Templating;

use Bloggist\Component\Renderer\RendererInterface;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface PostRendererInterface extends RendererInterface
{

    /**
     * @param boolean
     */
    public function setStubMode($stubMode);

}