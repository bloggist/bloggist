<?php

namespace Bloggist\Bundle\PostWebBundle\Templating;

use Bloggist\Component\Exception\InvalidTypeException;
use Bloggist\Component\Renderer\TemplateNotFoundException;
use Symfony\Component\Templating\EngineInterface;

/**
 * Description of PostRenderer
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostRenderer implements PostRendererInterface
{

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var array
     */
    private $templates = array();

    private $stubMode = false;

    public function __construct(EngineInterface $templating)
    {
        $this->templating = $templating;
    }

    public function setTemplate($type, $template)
    {
        $this->templates[$type] = $template;
    }

    public function render($object)
    {
        if (!$object instanceof \Bloggist\Component\Entity\Post) {
            throw new InvalidTypeException('Bloggist\Component\Entity\Post', $object);
        }

        if (\array_key_exists($object->getType(), $this->templates)) {
            $params = array(
                'post' => $object,
                'stub_mode' => $this->stubMode
            );

            return $this->templating->render($this->templates[$object->getType()], $params);

        } else {
            throw new TemplateNotFoundException("Template for post (type: {$object->getType()}) not found.");
        }
    }

    public function setStubMode($stubMode)
    {
        $this->stubMode = $stubMode;
    }

}