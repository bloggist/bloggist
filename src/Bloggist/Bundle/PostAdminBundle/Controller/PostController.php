<?php

namespace Bloggist\Bundle\PostAdminBundle\Controller;

use Bloggist\Bundle\PostAdminBundle\Form\Type\ArticleType;
use Bloggist\Bundle\PostAdminBundle\Form\Type\MediaType;
use Bloggist\Component\Entity\ArticlePost;
use Bloggist\Component\Entity\MediaPost;
use Bloggist\Component\Entity\Post;
use Bloggist\Component\Service\ServiceInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Templating\EngineInterface;

/**
 * Description of PostController
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostController
{
    private $request;

    private $formFactory;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var ServiceInterface
     */
    private $postService;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var string
     */
    private $formTemplate;

    /**
     * @var string
     */
    private $indexTemplate;

    public function __construct(
            Request $request,
            FormFactory $formFactory,
            EngineInterface $templating,
            ServiceInterface $postService,
            RouterInterface $router,
            /* string */ $formTemplate,
            /* string */ $indexTemplate)
    {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->templating = $templating;
        $this->postService = $postService;
        $this->router = $router;
        $this->formTemplate = $formTemplate;
        $this->indexTemplate = $indexTemplate;
    }

    public function index()
    {
        $posts = $this->postService->index(new \Bloggist\Component\Filter\PostFilter, 'id', 'desc');

        return $this->templating->renderResponse($this->indexTemplate, array('posts' => $posts));
    }

    public function create($type = 'Article')
    {
        if ($type === ArticlePost::TYPE) {
            $post = new ArticlePost;
        } elseif ($type === MediaPost::TYPE) {
            $post = new MediaPost;
        } else {
            \InvalidArgumentException("Invalid post type: " . $type);
        }

        $form = $this->createFormForPost($post);

        if ($this->request->isMethod('post')) {
            $form->bindRequest($this->request);

            if ($form->isValid()) {
                $this->postService->store($post);
            }

            $redirectUrl = $this->router->generate('bloggist_post_admin_edit', array('id' => $post->getId()));

            return new RedirectResponse($redirectUrl, 302);
        }

        return $this->templating->renderResponse($this->formTemplate, array('form' => $form->createView()));
    }

    public function edit($id)
    {
        $post = $this->postService->find($id);

        if (empty($post)) {
            throw new NotFoundHttpException("Post with id $id not found.");
        }

        $form = $this->createFormForPost($post);

        if ($this->request->isMethod('post')) {
            $form->bindRequest($this->request);

            if ($form->isValid()) {
                $this->postService->store($post);
            }
        }

        return $this->templating->renderResponse($this->formTemplate, array('form' => $form->createView()));
    }

    private function createFormForPost(Post $post)
    {
        if ($post instanceof ArticlePost) {
            return $this->formFactory->create(new ArticleType, $post);
        } elseif ($post instanceof MediaPost) {
            return $this->formFactory->create(new MediaType, $post);
        } else {
            throw new \InvalidArgumentException("Invalid post type: " . \get_class($post));
        }
    }
}