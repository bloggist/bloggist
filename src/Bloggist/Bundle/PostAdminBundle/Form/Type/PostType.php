<?php

namespace Bloggist\Bundle\PostAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Bloggist\Bundle\PostAdminBundle\Form\DataTransformer\StatusToStringTransformer;
use Bloggist\Bundle\PostAdminBundle\Form\DataTransformer\TagsToStringTransformer;

/**
 * Description of PostType
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
abstract class PostType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $statusTransformer = new StatusToStringTransformer();
        $tagsTransformer = new TagsToStringTransformer();

        $builder->add('title');
        $builder->add($builder->create('status', 'choice', array(
            'choices' => array('new' => 'New', 'complete' => 'Complete', 'disabled' => 'Disabled'),
            'required' => true,
        ))->addModelTransformer($statusTransformer));
        $builder->add('slug');
        $builder->add('synopsis');
        $builder->add($builder->create('tags', 'text')->addModelTransformer($tagsTransformer));
        $builder->add('postedAt', 'datetime', array('with_seconds' => false, 'date_widget' => 'single_text'));
    }

    public function getName()
    {
        return 'post';
    }
}