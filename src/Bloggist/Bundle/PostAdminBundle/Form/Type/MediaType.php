<?php

namespace Bloggist\Bundle\PostAdminBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of MediaType
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class MediaType extends PostType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('sourceType');
        $builder->add('resourceId');
        $builder->add('altText');
        $builder->add('caption');
        $builder->add('copyright');        
    }
    
    public function getName()
    {
        return 'media';
    }
}