<?php

namespace Bloggist\Bundle\PostAdminBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of ArticleType
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class ArticleType extends PostType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('content');
    }
    
    public function getName()
    {
        return 'article';
    }
}