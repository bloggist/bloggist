<?php

namespace Bloggist\Bundle\PostAdminBundle\Form\DataTransformer;

use Bloggist\Component\Entity\Tag;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Description of TagsToStringTransformer
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class TagsToStringTransformer implements DataTransformerInterface
{

    public function reverseTransform($value)
    {
        if (empty($value)) {
            return null;
        } else {
            $terms = \explode(",", $value);
            $callback = function($term) {
                $tag = new Tag;
                $tag->setName(preg_replace("/(^\s+)|(\s+$)/u", "", $term));
                return $tag;
            };
            
            return new ArrayCollection(\array_map($callback, \array_unique($terms)));
        }
    }

    public function transform($value)
    {
        if (!$value instanceof Collection || empty($value)) {
            return "";
        } else {
            $callback = function(Tag $tag) { return $tag->getName(); };
            return \implode(", ", \array_map($callback, $value->toArray()));
        }
    }

}