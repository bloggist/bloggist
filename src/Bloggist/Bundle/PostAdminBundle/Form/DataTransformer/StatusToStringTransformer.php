<?php

namespace Bloggist\Bundle\PostAdminBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Bloggist\Component\Entity\Status\Status;

/**
 * Description of StatusToStringTransformer
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class StatusToStringTransformer implements DataTransformerInterface
{
    public function reverseTransform($value)
    {
        if (empty($value)) {
            return null;
        } else {
            return new Status($value);
        }
    }

    public function transform($value)
    {
        if (!$value instanceof Status || empty($value)) {
            return "";
        } else {
            return $value->__toString();
        }
    }
}