<?php

namespace Bloggist\Bundle\FacebookBundle\Plugin\LikeButton;

use Bloggist\Component\Entity\Post;
use Bloggist\Component\Entity\MediaPost;
use Bloggist\Component\Plugin\ParametersResolverInterface;
use Bloggist\Component\Routing\RouterProxyInterface;

/**
 * Description of PostParametersResolver
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostParametersResolver implements ParametersResolverInterface
{
    /**
     * @var RouterProxyInterface
     */
    private $routerProxy;

    public function __construct(RouterProxyInterface $routerProxy)
    {
        $this->routerProxy = $routerProxy;
    }

    public function resolveParameters($object)
    {
        if (!$object instanceof Post) {
            throw new InvalidTypeException('Bloggist\Component\Entity\Post', $object);
        }

        /* @var $object Post */
        if ($object instanceof MediaPost) {
            $overrideParameters = array('hasPermalink' => false, 'colorscheme' => 'dark', 'layout' => 'button_count');
        } else {
            $overrideParameters = array();
        }
        
        $overrideParameters['url'] = $this->routerProxy->generate($object, true);
            
        return $overrideParameters;
    }

}