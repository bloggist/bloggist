<?php

namespace Bloggist\Bundle\FacebookBundle\Plugin\Comments;

use Bloggist\Component\Plugin\ParametersResolverInterface;
use Bloggist\Component\Routing\RouterProxyInterface;

/**
 * Description of PostParametersResolver
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostParametersResolver implements ParametersResolverInterface
{
    /**
     * @var RouterProxyInterface
     */
    private $routerProxy;

    public function __construct(RouterProxyInterface $routerProxy)
    {
        $this->routerProxy = $routerProxy;
    }

    public function resolveParameters($object)
    {
        $url = $this->routerProxy->generate($object);
        
        return array('url' => $url);
    }

}