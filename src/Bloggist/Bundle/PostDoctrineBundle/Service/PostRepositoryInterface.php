<?php

namespace Bloggist\Bundle\PostDoctrineBundle\Service;

use Bloggist\Component\Filter\PostFilterInterface;
use Bloggist\Component\Service\PageInterface;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface PostRepositoryInterface
{
    
    public function find($id);

    public function index(PostFilterInterface $filter, $sort, $order);

    public function paginate(PageInterface $page, $filter, $sort, $order);

}
