<?php

namespace Bloggist\Bundle\PageBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class PageController
{
    private $request;
    private $repository;
    private $templating;
    private $showTemplate;
    

    public function __construct(
            Request $request,
            EntityRepository $repository,
            EngineInterface $templating,
            $showTemplate)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->templating = $templating;
        $this->showTemplate = $showTemplate;
    }

    public function show($slug)
    {
        $page = $this->repository->findOneBy(array('slug' => $slug));

        if (empty($page)) {
            throw new NotFoundHttpException();
        }

        return $this->templating->renderResponse($this->showTemplate, array('page' => $page));
    }
}
